# Run Login
gcloud auth login
gcloud auth application-default login

# Set Project ID
echo Please Enter New Project ID:
read project_id
export project_id=$project_id
# Set Active Project
gcloud config set project $project_id

# Enable GCE K8 API
gcloud services enable container.googleapis.com

# Change to Terraform folder
cd terraform

# Variable Substitution
envsubst < variables.tf_template > variables.tf

# Clear Old Terraform State
rm -r terraform.tfstate.d

# Terraform Init
terraform init

# Terraform plan
terraform plan 

# Terraform apply
terraform apply

# Set Kubernettes cluster
gcloud container clusters get-credentials demo-cluster-zonal --zone europe-west2-a --project $project_id

# Ansibly Tasks - TODO

# Add Namespace
kubectl create ns gitlab-runner

# Create the Role
cat <<EOF | kubectl create -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: gitlab-runner
  namespace: gitlab-runner
rules:
  - apiGroups: [""]
    resources: ["pods"]
    verbs: ["list", "get", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["pods/exec"]
    verbs: ["create"]
  - apiGroups: [""]
    resources: ["pods/log"]
    verbs: ["get"]
EOF

# Bind the Role
kubectl create rolebinding --namespace=gitlab-runner gitlab-runner-binding --role=gitlab-runner --serviceaccount=gitlab-runner:default

# Install the helm chart
helm install --namespace gitlab-runner  gitlab-runner -f ./helm_runner/values.yaml gitlab/gitlab-runner
